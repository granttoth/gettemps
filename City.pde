//GLOBAL VARIABLES
Boolean staQualCheck;

class City {

  //COMPUTE GLOBALS
  String xmlURL;
  String city;
  String cityPrefix;
  String na = "NA";
  String TempFinal;
  String temp;
  //DISPLAY GLOBALS
  String tempstring;
  String TempStringError;
  Boolean error;

  //Send to TCP
  String tempFlex;

  //Icons & Temp Color
  PImage hotIcon;
  PImage coldIcon;

  //CONSTRUCTOR
  City(String _cityName, String _xmlURL) {
    city=_cityName;
    xmlURL=_xmlURL;
  }

  //FUNCTIONS
  void getTemps() {

    try {
      XML xml = loadXML(""+xmlURL+""); //XML source
      XML[] children = xml.getChildren("ob/variable");                     //where to dig in to
      String TempType = children[0].getString("description");              //what to grab
      String temp = children[0].getString("value");                        //what to grab
      String TempCheck = TempType + temp;            //Add the Data type and data for a line check

      //loop through XML file and check for the word OK for a station quality check.

      for (int i = 0; i < children.length; i++) {
        String[] m2 = match(""+children[i]+"", "OK");
        if (m2 != null) {
          staQualCheck = true;
        }
      }



      if ((TempCheck.equals("Temp"+temp) == true) && (staQualCheck) == true) {
        // if (TempCheck.equals("Temp"+temp)) {
        TempFinal = temp;       //Confirmed that the data is an actual temp
        error=false;            //used to check for the display
      } else {
        error=true;
      }
    }//end try
    catch (Exception s) {
      error=true;
      output.println("Network Connection Error for " + city); // Write the coordinate to the file
      output.flush(); // Writes the remaining data to the file
      //output.close(); // Finishes the file
    }
  }//end getTemps

  void display() {
    textFont(fontDefault, 26);
    if (error==false) {
      //text(city+" - "+TempFinal, width/2, displayPos);            //print to display
      textAlign(CENTER);                                     //center text
      fill(darkColor[0], darkColor[1], darkColor[2]);
      text(city, width/2-28, displayPos);            //print to display
      textAlign(RIGHT);
      text(" - ", width/2+42, displayPos);            //print to display
      textAlign(CENTER);                                     //center text
      tempColor();
      text(TempFinal, width/2+65, displayPos);            //print to display
      println(city + " - " + TempFinal);                          //print to console
    } else {
      temp = na;
      TempStringError = city+" - "+na;                              
      tempstring = city+" - " + temp;                             //combine city and temp for display
      text(tempstring, width/2, displayPos);                      //print "City - Temp" to display
      text(TempStringError, width/2, displayPos);                  //print to display
      println("Error Getting "+city+" Temp");                      //print to console
    }
    displayPos = displayPos + displaySpace;                         //increment the temp display position
  }//end display

  void tcpSend() {
    if (error==false) {
      tempFlex="~"+TempFinal+"|"+city;
      ClientPort[i].write(tempFlex);        //Send Temps to AudioVault
    } else if (error==true && ClientPort[i].active() == true) {
      ClientPort[i].write("~"+na+"|"+city); //Send to AudioVault
    }
  }

  void tempColor() {
    int tempColorInt = Integer.parseInt(TempFinal);
    if (tempColorInt >= -60 && tempColorInt <= -19) {
      fill(73, 32, 235);
    } else if (tempColorInt >= -20 && tempColorInt <= -9) {
      fill(55, 0, 235);
    } else if (tempColorInt >= -10 && tempColorInt <= 9) {
      fill(0, 0, 225);
    } else if (tempColorInt >= 10 && tempColorInt <= 19) {
      fill(0, 106, 235);
    } else if (tempColorInt >= 20 && tempColorInt <= 29) {
      fill(0, 162, 255);
    } else if (tempColorInt >= 30 && tempColorInt <= 39) {
      fill(0, 204, 255);
    } else if (tempColorInt >= 40 && tempColorInt <= 49) {
      fill(0, 235, 188);
    } else if (tempColorInt >= 50 && tempColorInt <= 59) {
      fill(0, 215, 36);
    } else if (tempColorInt >= 60 && tempColorInt <= 69) {
      fill(255, 174, 0);
    } else if (tempColorInt >= 70 && tempColorInt <= 79) {
      fill(255, 79, 0);
    } else if (tempColorInt >= 80 && tempColorInt <= 89) {
      fill(255, 79, 0);
    } else if (tempColorInt >= 90 && tempColorInt <= 99) {
      fill(255, 0, 0);
    } else if (tempColorInt >= 100) {
      fill(200, 0, 0);
    } else {
      fill(0);
    }
  }
}//end class