void timerDisplay() {
  for (i = 0; i < city.length; i++) {
      String secondsConvert = nf(seconds, 2);                                    //convert time to readable version
      String displayTime = "Check for update in: "+minutes+":"+secondsConvert;
      textAlign(CENTER);                                     //center text
      fill(lightColor[0], lightColor[1], lightColor[2]);                                                       //light background to match background
      rect(15, timerDisplayHeight-11, 260, 13); //background box refresh (upperleft pixel width, pixel height, box width, box height)
      fill(accentColor2[0], accentColor2[1], accentColor2[2]);//orange text color
      textFont(fontMono, 12);
      text(displayTime, width/2, timerDisplayHeight);
    
  }
}