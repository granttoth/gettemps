boolean noServer;
void serverCheck() {
  for (int i = 0; i < city.length; i++) {
    if (ClientPort[i].active() == false) {
      // Restets
      background(lightColor[0], lightColor[1], lightColor[2]);                             //light background at start to stop the gray flash
      begin = millis();                                           //reset timer
      time = duration;                                            //reset timer
      displayPos = displayPosOrig;    
      firstRun = true;
      noServer = true;
      makePorts();
    } else if (ClientPort[i].active() == true) {
      noServer = false;
    }
  }
}