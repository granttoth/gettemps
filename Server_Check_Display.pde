void serverCheckDisplay() {
  if (noServer == true) {
    String displayTime = "Server Disconnected... Please Wait.";
    textAlign(CENTER);                                     //center text
    fill(lightColor[0], lightColor[1], lightColor[2]);                                                       //light background to match background
    rect(15, height/2-11, 260, 13); //background box refresh (upperleft pixel width, pixel height, box width, box height)
    fill(errorColor[0], errorColor[1], errorColor[2]);//orange text color
    textFont(fontMono, 12);
    text(displayTime, width/2, height/2);
  }
}