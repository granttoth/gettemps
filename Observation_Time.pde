void ObserveTime() {
  try {
    //get time stamp
    //XML obxml = loadXML("http://www.BrokenURLtest.gov/mesowest/getobextXml.php?sid=KGCC&num=1");  // URL to get observation time from.  
    XML obxml = loadXML("http://www.wrh.noaa.gov/mesowest/getobextXml.php?sid=KGCC&num=1");  // URL to get observation time from.
    XML[] children = obxml.getChildren("ob");
    String time = children[0].getString("time");
    String ShortTime = time.substring(7);   
    fill(lightColor[0], lightColor[1], lightColor[2]);                                   //light background to match background
    rect(40, observationDisplayHeight-11, 220, 13);                                 //background box refresh (upperleft pixel width, pixel height, box width, box height)
    println("Observation time:", ShortTime);
    textAlign(CENTER);                                     //center text
    fill(accentColor2[0], accentColor2[1], accentColor2[2]);//orange
    textFont(fontMono, 12);
    String updatestring = "Observation Time: " + ShortTime+"";
    text(updatestring, width/2, observationDisplayHeight);
  }
  catch (Exception t) {
    textFont(fontMono, 12);
    String updatestring = "Error Getting Observation Time";
    text(updatestring, width/2, observationDisplayHeight);
    println("Error Getting Observation Time"); //print to console
    output.println("Error Getting Observation Time"); // Write the coordinate to the file
    output.flush(); // Writes the remaining data to the file
    //output.close(); // Finishes the file
  }
}