void version() {
  fill(lightColor[0], lightColor[1], lightColor[2]);                         //light background to match background
  rect(width-30, height-20, 40, 20); //background box refresh (upperleft pixel width, pixel height, box width, box height)
  fill(0);                         //light background to match background
  textFont(fontMono, 10);
  text(version, width-12, height-2);            //print to display
}