/*
 For questions contact Grant Toth. grant@sheridanmedia.com or granttoth@gmail.com.
 
 NOTES
 Working parts of the code is working but the display does not update.  
 I suspect that this is because there are no server active checks anywhere other than the server active bits.
 The timer stuff happens even though there is no server.
 Mabye make everything check for a server connection.
 */

String version ="v2.1";


// the index locations of the city needs to match other indices
String [] city= {
  "Sheridan", 
  "Buffalo", 
  "Gillette"        //no end comma
}; 
String [] xml = {
  "http://www.wrh.noaa.gov/mesowest/getobextXml.php?sid=KSHR&num=1", //Sheridan - index 0
  //"http://www.brokenURL.gov/mesowest/getobextXml.php?sid=KSHR&num=1", //Sheridan - index 0
  "http://www.wrh.noaa.gov/mesowest/getobextXml.php?sid=KBYG&num=1", //Buffalo  - index 1
  "http://www.wrh.noaa.gov/mesowest/getobextXml.php?sid=KGCC&num=1"  //Gillette - index 2
}; 
int [] tcpPort = {
  5010, // TCP port for Sheridan
  5020, // Buffalo
  5030  //Gillette
};

//Display Locations
int time = duration = 150;                      // update time in seconds

int areaTempsHeight = 45;                       //height for the "Area Temps" header
int displayPos = 100;                            //set the spacing of the temps on screen
int displaySpace = 30;                          //set the vertical placement of the temps on screen
int timerDisplayHeight = 215;                   //height of the timer clock
int observationDisplayHeight = 195;             //height of the observation display

//Colors
int [] lightColor = {248, 248, 248};           //Light
int [] darkColor = {34, 30, 29};               //Dark
int [] accentColor1 = {43, 173, 217};          //blue
int [] accentColor2 = {0, 190, 0};            //green
int [] errorColor = {208, 0, 0};            //green

//int lc1 = 233; int lc2 = 233; int lc3 = 233;

///////DO NOT EDIT BELLOW///////
import processing.net.*; 


int i;
PFont fontH1;
PFont fontDefault;
PFont fontMono;
boolean firstRun = true;
int displayPosOrig = displayPos;                //get original display position value.
int begin; 
int duration;
//String displayTime;
int seconds;
int minutes;
//String secondsConvert;

PrintWriter output;  //Log File


Client[] ClientPort; //clients as array objects
City myCity; //declair a new instance of the City class

void keyPressed() { //disable the esc key
  if (key==27)
    key=0;
}

void setup() {
  keyPressed();                                          //disable esc key
  //thread("makePorts");
  makePorts();
  output = createWriter("ErrorLog.txt");                 //error log fill
  frameRate(1);                                         //set frame rate
  size(300, 230);                                        //window size
  background(lightColor[0], lightColor[1], lightColor[2]);                             //light background at start to stop the gray flash
  fontH1 = loadFont("Candara-Bold-32.vlw");              //load fonts
  fontDefault = loadFont("Calibri-26.vlw");              //load fonts
  fontMono = loadFont("LucidaSans-Typewriter-12.vlw");   //load font 
  textFont(fontDefault, 32);                                    //set default text font
  smooth();                                              //shape and font smoothing
  noStroke();                                            //no stroke around shapes
}//end setup


void draw() {
  serverCheck();

  timerCounter();                               // math for the timer
  if (noServer == false) {
    timerDisplay();                               // display for the timer  
    displayHeader();                              // place the header
  }


  if (firstRun==true && noServer == false) {
    for (i = 0; i < city.length; i++) {         //iterate through the cities and xml feeds to get data from
      myCity = new City(city[i], xml[i]);      //Initialize the CLASS INSTANCE 
      myCity.getTemps();                        // a function of the class to get the temps
      myCity.display();                         // a function of the class to print temps to display and console
      myCity.tcpSend();                         // a function of the class to send data to the TCP connection
      firstRun=false;                           // not the first run anymore.. set to false till next startup
    }
    ObserveTime();
  } else {
    serverCheckDisplay();
  }

  if (time<=0 && noServer == false) {

    //reset the timer here
    begin = millis();                                           //reset timer
    time = duration;                                            //reset timer
    displayPos = displayPosOrig;                                //reset display positions from the iterations.
    background(lightColor[0], lightColor[1], lightColor[2]);    //to refresh and clear the previous text

    for (i = 0; i < city.length; i++) {         //iterate through the cities and xml feeds to get data from
      myCity = new City(city[i], xml[i]);       //Initialize the CLASS INSTANCE 
      myCity.getTemps();                        // a function of the class to get the temps
      myCity.display();                         // a function of the class to print temps to display and console
      myCity.tcpSend();                         // a function of the class to send data to the TCP connection
    }
    ObserveTime();
  }
  version();
}//end draw