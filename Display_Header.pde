void displayHeader() {
  textAlign(CENTER);                                     //center text
  fill(lightColor[0], lightColor[1], lightColor[2]);                         //light background to match background
  rect(10, areaTempsHeight-34, width-20, 45);  //(upperleft pixel width, pixel height, box width, box height)
  textFont(fontH1, 35);
  //textSize(35);
  fill(accentColor1[0], accentColor1[1], accentColor1[2]);  //blue
  text("Area Temps", width/2, areaTempsHeight);
}